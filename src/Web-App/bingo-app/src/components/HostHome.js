import React from "react";
import HostNavBar from "./HostNavBar";

const HostHome = () => {
    return ( 
    <>
    <HostNavBar/>
    <h1>We are in your Host Home Page</h1>
    </>
    )

};

export default HostHome;